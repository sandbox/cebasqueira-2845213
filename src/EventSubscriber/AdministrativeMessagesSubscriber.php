<?php

namespace Drupal\administrative_messages\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\administrative_messages\AdministrativeMessagesManager;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;

/**
 * Class AdministrativeMessagesSubscriber.
 *
 * @package Drupal\administrative_messages
 */
class AdministrativeMessagesSubscriber implements EventSubscriberInterface {

  /**
   * Check if current user have messages.
   */
  public function checkForMessages(GetResponseEvent $event) {
    global $base_root;

    $manager = new AdministrativeMessagesManager();
    $has_message = $manager->userHasMessages();

    $current_url = Url::fromRoute('<current>');
    $current_page = $base_root . $current_url->toString();
    $message_page = Url::fromRoute('administrative_messages.list')->toString();

    // If user has messages no readed show the warning message.
    if ($has_message && $current_page != $base_root . $message_page) {

      // Replace the token for list link.
      $token_service = \Drupal::token();
      $warning_message = \Drupal::config('administrative_messages.settings')->get('administrative_messages_warning');
      $warning_message = $token_service->replace($warning_message, array('message-list' => $message_page));
      drupal_set_message(Markup::create($warning_message), 'warning');
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkForMessages');
    return $events;
  }

}
