<?php

namespace Drupal\administrative_messages;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Render\Markup;

/**
 * Class AdministrativeMessagesManager.
 *
 * @package Drupal\administrative_messages
 */
class AdministrativeMessagesManager {

  /**
   * Function that returns if logged user has messages.
   *
   * @return boll
   *    true or false.
   */
  public function userHasMessages() {

    $messages = $this->getUserMessages();
    $has_message = FALSE;

    if ($messages) {
      $has_message = TRUE;
    }
    return $has_message;
  }

  /**
   * Function that returns user messages.
   *
   * @param bool $unread
   *   The id of the message.
   *
   * @return array
   *    Array with messages.
   */
  public function getUserMessages($mid = NULL, $unread = TRUE) {
    $query = \Drupal::database()->select('administrative_messages_list', 'a');
    $query->join('users_field_data', 'ufd', 'ufd.uid = a.message_from');
    $query->fields('a', array(
      'message_id',
      'message_read',
      'message_sent',
      'message_body',
    ));
    $query->condition('message_to', \Drupal::currentUser()->id());
    $query->fields('ufd', array('name', 'mail'));
    if ($unread && empty($mid)) {
      $query->isNull('message_read');
    }
    if (!empty($mid)) {
      $query->condition('message_id', $mid);
    }
    $query->orderBy('message_sent', 'DESC');
    $messages = $query->execute()->fetchAll();

    return $messages;
  }

  /**
   * Function that returns data of the message.
   *
   * @param int $mid
   *   The id of the message.
   *
   * @return string
   *    Array with messages.
   */
  public function getMessage($mid) {

    $data = $this->getUserMessages($mid);

    $message_data = t('<strong>Message sent by: </strong> @name on (@sent)', array('@name' => $data[0]->name, '@sent' => $data[0]->message_sent));

    if ($data[0]->message_read) {
      $message_data = t('<strong>Message marked as read on: </strong> @read', array('@read' => $data[0]->message_read));
    }

    $message_data = t('<strong>Message sent by: </strong> @name on (@sent)', array('@name' => $data[0]->name, '@sent' => $data[0]->message_sent));
    $message_data .= t('<br><strong>Message:</strong><br>@message<br><br>', array('@message' => Markup::create(nl2br($data[0]->message_body))));

    return $message_data;
  }

  /**
   * Function that returns data for list/history messages list.
   *
   * @param bool $unread
   *   Unread messages?.
   *
   * @return array
   *    Array with list of messages.
   */
  public function geTableMessageData($unread = TRUE) {
    $data = $this->getUserMessages(NULL, $unread);

    foreach ($data as $value) {
      $out[$value->message_id]['message_id'] = $value->message_id;
      $out[$value->message_id]['message_sent'] = $value->message_sent;
      $out[$value->message_id]['message_read'] = (!empty($value->message_read)) ? $value->message_read : '-';
      $out[$value->message_id]['message_from'] = $value->name;
      $out[$value->message_id]['message_body'] = Unicode::truncate($value->message_body, 145, TRUE, TRUE);
    }

    return $out;
  }

}
