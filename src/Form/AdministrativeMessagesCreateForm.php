<?php

namespace Drupal\administrative_messages\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Database\Database;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Render\Markup;

/**
 * Implements an AdministrativeMessagesCreateForm form.
 */
class AdministrativeMessagesCreateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'administrative_messages_create_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('administrative_messages.settings');

    $form['to'] = array(
      '#title' => $this->t('Message to'),
      '#type' => 'select',
      '#options' => $this->getUsers(),
      '#description' => $this->t('This message will be sent only by admin system, not for e-mail.'),
    );

    if ((bool) $config->get('administrative_messages_email')) {
      $form['by_email'] = array(
        '#title' => $this->t('Send a copy by e-mail.'),
        '#type' => 'checkbox',
        '#default_value' => FALSE,
      );
    }

    $form['message'] = array(
      '#title' => $this->t('Body message'),
      '#type' => 'textarea',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Send Message'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('to')) {
      $form_state->setErrorByName('to', $this->t('Select the message recipient.'));
    }
    if (!$form_state->getValue('message')) {
      $form_state->setErrorByName('message', $this->t('The message can not be empty.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('administrative_messages.settings');
    $conn = Database::getConnection();
    $conn->insert('administrative_messages_list')->fields(
      array(
        'message_id' => NULL,
        'message_to' => (int) $form_state->getValue('to'),
        'message_from' => (int) \Drupal::currentUser()->id(),
        'message_read' => NULL,
        'message_sent' => date('Y-m-d H:i:s', time()),
        'message_body' => Xss::filter($form_state->getValue('message')),
      )
    )->execute();

    if ((bool) $config->get('administrative_messages_email') && $form_state->getValue('by_email')) {
      $this->sendMessageMail(
        (int) $form_state->getValue('to'),
        Xss::filter($form_state->getValue('message'))
      );
    }

    drupal_set_message($this->t('The message has been sent. The user will receive the message if he is logged in now or at the next login.'));
  }

  /**
   * {@inheritdoc}
   */
  private function getUsers() {

    $users = User::loadMultiple();
    $current_user = \Drupal::currentUser()->id();
    $option_users = array('' => $this->t('Select a user'));

    foreach (array_keys($users) as $uid) {
      if ($uid == 0 || $uid == $current_user) {
        continue;
      }
      $option_users[$uid] = $users[$uid]->name->value . ' (' . $users[$uid]->mail->value . ')';
    }

    return $option_users;
  }

  /**
   * {@inheritdoc}
   */
  private function sendMessageMail($to, $message) {
    $config = $this->config('administrative_messages.settings');
    $to = user_load($to);
    $to = $to->mail->value;

    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'administrative_messages';
    $key = 'message-create';
    $params['message'] = $this->prepareMessage($message);
    $params['title'] = User::load(\Drupal::currentUser()->id())->get('mail')->value;
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;

    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] != TRUE) {
      $message = $this->t('There was a problem sending your email notification to @email.', array('@email' => $to));
      drupal_set_message($message, 'error');
      \Drupal::logger('mail-log')->error($message);
      return;
    }

    $token_service = \Drupal::token();
    $send_message = $config->get('administrative_messages_subject');
    $send_message = $token_service->replace($send_message, array('mail-message' => $to));

    drupal_set_message($send_message);
    \Drupal::logger('mail-log')->notice($send_message);
  }

  /**
   * {@inheritdoc}
   */
  private function prepareMessage($message) {

    global $base_url;

    $message .= $this->t('<br><br>More at: @link', array('@link' => $base_url));
    $message = Markup::create(nl2br($message));

    return $message;
  }

}
