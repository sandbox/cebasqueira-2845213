<?php

namespace Drupal\administrative_messages\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\administrative_messages\AdministrativeMessagesManager;
use Drupal\Core\Url;
use Drupal\Core\Database\Database;

/**
 * Implements an AdministrativeMessagesReadForm form.
 */
class AdministrativeMessagesReadForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'administrative_messages_read_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $mid = NULL) {
    $manager = new AdministrativeMessagesManager();
    $message_data = $manager->getMessage($mid);

    $form['message_data'] = array(
      '#markup' => $message_data,
    );

    $form['message_id'] = array(
      '#type' => 'hidden',
      '#value' => $mid,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Mark as read'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $connection = Database::getConnection();
    $query = $connection->update('administrative_messages_list')
      ->fields(
      array(
        'message_read' => date('Y-m-d H:i:s', time()),
      )
    )
      ->condition('message_id', $form_state->getValue('message_id'));
    $query->execute();

    drupal_set_message($this->t('The message has been marked as readed.'));

    $url = Url::fromRoute('administrative_messages.list');
    return $form_state->setRedirectUrl($url);
  }

}
