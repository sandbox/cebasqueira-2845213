<?php

namespace Drupal\administrative_messages\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\administrative_messages\AdministrativeMessagesManager;
use Drupal\Core\Url;

/**
 * Implements an AdministrativeMessagesHistoryForm form.
 */
class AdministrativeMessagesHistoryForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'administrative_messages_history_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $mid = NULL) {
    $manager = new AdministrativeMessagesManager();

    $form['help'] = array(
      '#markup' => $this->t('<br>This page show all messages sent by you included the unread messages.<br><br>'),
    );

    $form['table'] = array(
      '#type' => 'tableselect',
      '#header' => array(
        'message_id' => $this->t('Message ID'),
        'message_sent' => $this->t('Send Date'),
        'message_read' => $this->t('Read Date'),
        'message_from' => $this->t('From (User)'),
        'message_body' => $this->t('Message...'),
      ),
      '#options' => $manager->geTableMessageData(FALSE),
      '#empty' => $this->t('No messages found'),
      '#multiple' => FALSE,
    );

    if ($manager->geTableMessageData(FALSE)) {
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Show selected message'),
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $url = Url::fromRoute('administrative_messages.read', array('mid' => $form_state->getValue('table')));
    return $form_state->setRedirectUrl($url);
  }

}
