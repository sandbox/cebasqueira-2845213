<?php

namespace Drupal\administrative_messages\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements an AdministrativeMessagesConfigForm form.
 */
class AdministrativeMessagesConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'administrative_messages_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['administrative_messages.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('administrative_messages.settings');

    $form['default_values'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Default values'),
    );

    $form['default_values']['administrative_messages_warning'] = array(
      '#type' => 'textfield',
      '#maxlength' => 200,
      '#title' => $this->t('New message warning'),
      '#description' => $this->t('Set the default message when the user recieve a new message. Use [administrative-messages:message:list] to link to the list messages page.'),
      '#default_value' => $config->get('administrative_messages_warning'),
    );

    $form['default_values']['administrative_messages_subject'] = array(
      '#type' => 'textfield',
      '#maxlength' => 200,
      '#title' => $this->t('E-mail subject message'),
      '#description' => $this->t('Set the default subject for e-mail message. Use [administrative-messages:mail:from].'),
      '#default_value' => $config->get('administrative_messages_subject'),
    );

    $form['default_values']['administrative_messages_message'] = array(
      '#type' => 'textfield',
      '#maxlength' => 200,
      '#title' => $this->t('E-mail success message'),
      '#description' => $this->t('Set the default message for e-mail success message. Use [administrative-messages:mail:message].'),
      '#default_value' => $config->get('administrative_messages_message'),
    );

    $form['default_values']['administrative_messages_email'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable "Send copy by e-mail" feature'),
      '#default_value' => $config->get('administrative_messages_email'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('administrative_messages_warning'))) {
      $form_state->setErrorByName('administrative_messages_warning', $this->t('Warning Message is empty.'));
    }
    if (empty($form_state->getValue('administrative_messages_subject'))) {
      $form_state->setErrorByName('administrative_messages_subject', $this->t('Subject is empty.'));
    }
    if (empty($form_state->getValue('administrative_messages_message'))) {
      $form_state->setErrorByName('administrative_messages_message', $this->t('Message is empty.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('administrative_messages.settings');
    foreach ($form_state->getValues() as $key => $value) {
      if (strpos($key, 'administrative_messages_') !== FALSE) {
        $config->set($key, $value);
      }
    }

    $config->save();

    drupal_set_message($this->t('Administrative Messages configuration has been saved.'));
  }

}
