<?php

/**
 * @file
 * Administrative Messages module file.
 */

use Drupal\Core\Render\BubbleableMetadata;


/**
 * Implements hook_page_attachments().
 */
function administrative_messages_page_attachments(array &$attachments) {
  if (!\Drupal::currentUser()->hasPermission('access contextual links')) {
    return;
  }

  $attachments['#attached']['library'][] = 'administrative_messages/administrative_messages_toolbar';
}

/**
 * Implements hook_token_info().
 */
function administrative_messages_token_info() {
  $type = array(
    'name' => t('Administrative Messages'),
    'description' => t('Tokens related to Administrative Messages.'),
    'needs-data' => 'administrative_messages',
  );

  $communication_mail['message:list'] = array(
    'name' => t("Messages List"),
    'description' => t("The list of unread messages."),
  );

  $communication_mail['message:history'] = array(
    'name' => t("Messages History"),
    'description' => t("The list of history of messages."),
  );

  $communication_mail['mail:from'] = array(
    'name' => t("Mail from"),
    'description' => t("The e-mail from."),
  );

  $communication_mail['mail:message'] = array(
    'name' => t("Mail successfull message"),
    'description' => t("The message that be displayed when e-mail is sent successfully."),
  );

  return array(
    'types' => array('administrative-messages' => $type),
    'tokens' => array('administrative-messages' => $communication_mail),
  );
}

/**
 * Implements hook_tokens().
 */
function administrative_messages_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = array();

  if ($type == 'administrative-messages') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'message:list':
          $replacements[$original] = $data['message-list'];
          break;

        case 'message:history':
          $replacements[$original] = $data['message-history'];
          break;

        case 'mail:from':
          $replacements[$original] = $data['mail-from'];
          break;

        case 'mail:message':
          $replacements[$original] = $data['mail-message'];
          break;
      }
    }
  }

  return $replacements;
}

/**
 * Implements hook_mail().
 */
function administrative_messages_mail($key, &$message, $params) {
  switch ($key) {
    case 'message-create':

      $token_service = \Drupal::token();
      $subject_message = \Drupal::config('administrative_messages.settings')->get('administrative_messages_subject');
      $subject_message = $token_service->replace($subject_message, array('mail-from' => $params['title']));

      $message['subject'] = $subject_message;
      $message['body'][] = $params['message'];

      break;
  }

}
